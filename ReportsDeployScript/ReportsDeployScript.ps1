﻿<#
.SYNOPSIS
Deploys SSRS reports and other artefacts to SSRS server.
Also creates a single data source and associates all reports with that source.

.EXAMPLE
DeployReports.ps1 -reportServerName us156sq-winvm01 -fromDirectory . -serverPath "/RUL" -dataSourceDbServer us156uq-azsql1 -dataSourceDb Weir.Synertrex.MpsCentral.Db -dataSourceDbUserName us156uq-azsql1admin -dataSourceDbPassword blah

#>
[CmdletBinding()]
Param(
	[parameter(Mandatory=$true)]
	[string] $reportServerName,
	[parameter(Mandatory=$true)]
	[string] $fromDirectory,
	[parameter(Mandatory=$true)]
	[string] $serverPath,
	[parameter(Mandatory=$true)]
	[string] $dataSourceDbServer,
	[parameter(Mandatory=$true)]
	[string] $dataSourceDb,
	[parameter(Mandatory=$true)]
	[string] $dataSourceDbUserName,
	[parameter(Mandatory=$true)]
	[string] $dataSourceDbPassword,
	[parameter(Mandatory=$true)]
	[string] $SsrsVmUserName,
	[parameter(Mandatory=$true)]
	[string] $SsrsVmPassword
)
 
function UploadCatalogItem([string] $itemType, $files)
{
    $uploadedCount = 0
     
    foreach ($fileInfo in $files)
    {    
        $file = [System.IO.Path]::GetFileNameWithoutExtension($fileInfo.FullName)

        $percentDone = (($uploadedCount/$files.Count) * 100)        
        Write-Progress -activity "Uploading to $reportServerName$serverPath" -status $file -percentComplete $percentDone
        Write-Output "%$percentDone : Uploading $file to $reportServerName$serverPath"
        $bytes = [System.IO.File]::ReadAllBytes($fileInfo.FullName)
        $warnings = @{}
        $catalogItem = $proxy.CreateCatalogItem($itemType, $file, $serverPath, $true, $bytes, $null, [ref]$warnings)
         
        if ($warnings)
        {
            foreach ($warn in $warnings)
            {
                Write-Warning $warn.Message
            }
        }
         
        $uploadedCount += 1
    }    
}

$dataSourceName = "AdventureWorksCentral"
$SsrsVmPasswordEncrypted = ConvertTo-SecureString $SsrsVmPassword -AsPlainText -Force
$SsrsVmCreds = New-Object System.Management.Automation.PSCredential ($SsrsVmUserName, $SsrsVmPasswordEncrypted)

Write-Output "Connecting to $reportServerName"
$reportServerUri = "http://{0}/ReportServer/ReportService2010.asmx" -f $reportServerName
$proxy = New-WebServiceProxy -Uri $reportServerUri -Credential $SsrsVmCreds
$proxyNamespace = $proxy.GetType().Namespace

# Create report folder

Write-Output "Creating folder $serverPath"
$parentFolder = Split-Path $serverPath -Parent
$parentFolder = $parentFolder.replace("\","/")
$folder = Split-Path $serverPath -Leaf

$catalogItem = $proxy.CreateFolder($folder, $parentFolder, $null)

# Create data source

Write-Output "Creating data source"

$newDataSourceDefinition = New-Object ("$proxyNamespace.DataSourceDefinition") 
$newDataSourceDefinition.ConnectString  = "Data Source=$($dataSourceDbServer).database.windows.net;Initial Catalog=$($dataSourceDb);Encrypt=True;TrustServerCertificate=False"
$newDataSourceDefinition.Extension  = "SQL"
$newDataSourceDefinition.UserName = $dataSourceDbUserName
$newDataSourceDefinition.Password = $dataSourceDbPassword
$newDataSourceDefinition.WindowsCredentials  = $false
$newDataSourceDefinition.CredentialRetrieval = "Store"

$proxy.CreateDataSource($dataSourceName, $serverPath, $true, $newDataSourceDefinition, $null)

# Upload all artefacts
     
Write-Output "Inspecting $fromDirectory"

$files = @(get-childitem $fromDirectory * -rec|where-object {!($_.psiscontainer)})

Write-Output "Uploading data sets"
$dataSets = @($files | where-object { [System.IO.Path]::GetExtension($_) -eq ".rsd" })
UploadCatalogItem "DataSet" $dataSets 

Write-Output "Uploading reports"
$reports = @($files | where-object { [System.IO.Path]::GetExtension($_) -eq ".rdl" })
UploadCatalogItem "Report" $reports

# Associate reports with data source 

$sharedDataSource = New-Object ("$proxyNamespace.DataSource") 
$sharedDataSource[0].Name = $dataSourceName
$sharedDataSource[0].Item = New-Object ("$proxyNamespace.DataSourceReference")
$sharedDataSource[0].Item.Reference = "$($serverPath)/$($dataSourceName)"

$reports = $proxy.ListChildren($serverPath, $false) | where-object {($_.TypeName -eq "Report") }
  
foreach ($report in $reports)
{
    $reportPath = $report.path
    Write-Host "Report: " $reportPath
    $dataSources = $proxy.GetItemDataSources($reportPath)
    foreach ($dataSource in $dataSources)
    {
        Write-Host "Old source: $($dataSource.Name), $($dataSource.Item.Reference)"
		#Write-Host "Argument1" + $reportPath
		#Write-Host "Argument2" + $sharedDataSource
        $proxy.SetItemDataSources($reportPath, @($sharedDataSource))
        Write-Host "New source: $($dataSource.Name), $($dataSource.Item.Reference)"
    }
}